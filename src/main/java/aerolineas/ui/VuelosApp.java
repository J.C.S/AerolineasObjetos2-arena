package aerolineas.ui;

import Airlines.VueloStore;

public class VuelosApp {

	public static void main(String[] args) {

		VueloStore.store().iniciarVuelo();

		new VuelosWindow(VueloStore.store()).startApplication();
	}
}
