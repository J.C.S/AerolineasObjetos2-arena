package aerolineas.ui;




import java.time.LocalDate;

import org.uqbar.arena.bindings.ObservableProperty;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.TextBox;
import org.uqbar.arena.windows.Dialog;
import org.uqbar.arena.windows.WindowOwner;
import org.uqbar.commons.model.ObservableUtils;

import Airlines.Pasaje;
import TipoDeVuelo.VueloGeneral;

public class AgregarPasaporteWindow extends Dialog<Pasaje>{

	
	private static final long serialVersionUID = 1L;
	public VueloGeneral vuelo;
	public Pasaje pasaje;
	
	public AgregarPasaporteWindow(WindowOwner owner, Pasaje model, VueloGeneral vueloGeneral) {
		super(owner, model);
		this.vuelo = vueloGeneral;
		this.pasaje = new Pasaje();
	}

	
	@Override
	protected void createFormPanel(Panel arg0) {
		
		new Label(arg0)
		.setText("DNI");
		
		new TextBox(arg0)
		.bindValueToProperty("dni");
		
		new Label(arg0)
		.setText("Nombre Y Apellido");
		
		new TextBox(arg0)
		.bindValueToProperty("nombre");
		
		new Label(arg0)
		.setText("Fecha de compra");
		
		new Label(arg0)
		.bindValue(new ObservableProperty<>(this, "fechaActual"));
		
		new Label(arg0)
		.setText("Precio a abonar");
		
		new Label(arg0)
		.bindValue(new ObservableProperty<>(vuelo, "precioDeVenta"));
		
		Button cancel = new Button(arg0);
		cancel.setCaption("Cancelar");
		cancel.onClick(() -> close());
		
		Button accept = new Button(arg0);
		accept.setCaption("Aceptar");
		accept.onClick(() -> { 
			this.vuelo.venderPasaje(new Pasaje(this.getModelObject().getDni(),
						this.getModelObject().getNombre(), 
						this.fechaActual()), 
						this.vuelo);
			ObservableUtils.firePropertyChanged(this.vuelo, "cantidadDeAsientosLibres");
			close();
		});
	}
	
	public LocalDate fechaActual(){
		return LocalDate.now();
	}

}
