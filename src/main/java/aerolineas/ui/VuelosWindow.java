package aerolineas.ui;

import org.uqbar.arena.bindings.ObservableProperty;
import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.GroupPanel;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.tables.Column;
import org.uqbar.arena.widgets.tables.Table;
import org.uqbar.arena.windows.MainWindow;

import Airlines.VueloStore;
import TipoDeVuelo.VueloGeneral;

public class VuelosWindow extends MainWindow<VueloStore>{
	public VuelosWindow(VueloStore model) {
		super(model);
	}
	private static final long serialVersionUID = 1L;

	private ControllerDelVuelo seleccionado = new ControllerDelVuelo();

	@Override
	public void createContents(Panel main) {
		
		
		
		Table<VueloGeneral> flights = new Table<>(main, VueloGeneral.class);
		
		flights.bindItemsToProperty("vuelos");
		flights.setNumberVisibleRows(4);
		flights.bindValue(
				new ObservableProperty<Object>(seleccionado,
						"componenteSeleccionado"));
		
		new Column<VueloGeneral>(flights)
		.setTitle("Fecha")
		.setFixedSize(90)
		.bindContentsToProperty("fechaDeVuelo");
		
		
		new Column<VueloGeneral>(flights)
			.setTitle("Origen")
			.setFixedSize(80)
			.bindContentsToProperty("origen");
		
		new Column<VueloGeneral>(flights)
		.setTitle("Destino")
		.setFixedSize(80)
		.bindContentsToProperty("destino");
		
		new Column<VueloGeneral>(flights)
		.setTitle("Tipo")
		.setFixedSize(80)
		.bindContentsToProperty("tipo");
		
		new Column<VueloGeneral>(flights)
		.setTitle("Asientos Libres")
		.setFixedSize(110)
		.bindContentsToProperty("cantidadDeAsientosLibres");
		
		new Column<VueloGeneral>(flights)
		.setTitle("Precio Estandar")
		.setFixedSize(110)
		.bindContentsToProperty("precioEstandar");
		
		new Column<VueloGeneral>(flights)
		.setTitle("Precio Actual")
		.setFixedSize(100)
		.bindContentsToProperty("precioDeVenta");
		
		new Column<VueloGeneral>(flights)
		.setTitle("A la venta")
		.setFixedSize(50)
		.bindContentsToProperty("estaALaVenta");
		
		mostrarInfoAvion(main);

		Button abrirVentana = new Button(main);
		abrirVentana.setCaption("Ver detalles del vuelo");
		abrirVentana.onClick(() -> new detalleDelVueloWindow(this, seleccionado.getComponenteSeleccionado(),this).open());
		
				
		
	
	}

	protected void mostrarInfoAvion(Panel main) {
		GroupPanel panelflight = new GroupPanel(main);
		panelflight.setTitle("Informacion del avion");
		
		
		Label planename = new Label(panelflight);
		planename.bindValue(new ObservableProperty<>(seleccionado, "componenteSeleccionado.avion.nombre"));		
		planename.setFontSize(15);
		
		
		Panel labelb = new Panel(panelflight);

		labelb.setLayout(new ColumnLayout(4));
		
		
		new Label(labelb)
			.setText("Cantidad total de asientos: ");
		new Label(labelb)
			.bindValue((new ObservableProperty<Object>(seleccionado, "componenteSeleccionado.cantidadDeAsientosEnTotal")));
		
		new Label(labelb)
		.setText("Peso: ");
		new Label(labelb)
		.bindValue((new ObservableProperty<Object>(seleccionado, "componenteSeleccionado.avion.pesoDelAvion")));

		new Label(labelb)
		.setText("Altura de la cabina: ");
		new Label(labelb)
		.bindValue((new ObservableProperty<Object>(seleccionado, "componenteSeleccionado.avion.alturaCabina")));

		new Label(labelb)
		.setText("Consumo de nafta por kilometro: ");
		new Label(labelb)
		.bindValue((new ObservableProperty<Object>(seleccionado, "componenteSeleccionado.avion.consumoDeNaftaPorKm")));
	}

}
