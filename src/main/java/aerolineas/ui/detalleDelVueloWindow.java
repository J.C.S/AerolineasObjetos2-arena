package aerolineas.ui;


import java.time.LocalDate;

import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.GroupPanel;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.TextBox;
import org.uqbar.arena.widgets.tables.Column;
import org.uqbar.arena.widgets.tables.Table;
import org.uqbar.arena.windows.Window;
import org.uqbar.arena.windows.WindowOwner;

import Airlines.Pasaje;
import TipoDeVuelo.VueloGeneral;

public class detalleDelVueloWindow extends Window<VueloGeneral> {

	private static final long serialVersionUID = 1L;
	private VuelosWindow window;

	public detalleDelVueloWindow(WindowOwner owner, VueloGeneral model,VuelosWindow window) {
		super(owner, model);
		this.window = window;
	}

	
	@Override
	public void createContents(Panel arg0) {

		
		GroupPanel panelflight = new GroupPanel(arg0);
		panelflight.setTitle("Informacion del vuelo");
		panelflight.setLayout(new ColumnLayout(4));
		
		new Label(panelflight)
		.setText("Fecha: ");
		new Label(panelflight).bindValueToProperty("fechaDeVuelo");

		new Label(panelflight)
		.setText("Origen: ");
		new Label(panelflight).bindValueToProperty("origen");
		
		new Label(panelflight)
		.setText("Destino: ");
		new Label(panelflight).bindValueToProperty("destino");
		
		new Label(panelflight)
		.setText("Tipo: ");
		new Label(panelflight).bindValueToProperty("tipo");
		
		new Label(panelflight)
		.setText("Asientos libres: ");
		new Label(panelflight).bindValueToProperty("cantidadDeAsientosLibres");
		
		new Label(panelflight)
		.setText("Precio estandar: ");
		new Label(panelflight).bindValueToProperty("precioEstandar");
		
		new Label(panelflight)
		.setText("Precio actual: ");
		new Label(panelflight).bindValueToProperty("precioDeVenta");
		
		new Label(panelflight)
		.setText("¿A la venta?: ");
		new Label(panelflight).bindValueToProperty("estaALaVenta");
		
		
		window.mostrarInfoAvion(arg0);
		
		
		Table<Pasaje> flightattendant = new Table<>(arg0, Pasaje.class);
		
		flightattendant.bindItemsToProperty("pasajes");
		flightattendant.setNumberVisibleRows(4);
		
		new Column<Pasaje>(flightattendant)
		.setTitle("DNI")
		.setFixedSize(80)
		.bindContentsToProperty("dni");
		
		new Column<Pasaje>(flightattendant)
		.setTitle("Nombre y Apellido")
		.setFixedSize(80)
		.bindContentsToProperty("nombre");
		
		new Column<Pasaje>(flightattendant)
		.setTitle("Fecha de venta")
		.setFixedSize(80)
		.bindContentsToProperty("fechaDeVenta");
		
		new Column<Pasaje>(flightattendant)
		.setTitle("Precio abonado")
		.setFixedSize(80)
		.bindContentsToProperty("precioComprado");
		
		Button abrirVentana = new Button(arg0);
		abrirVentana.setCaption("Agregar Pasajero");
		abrirVentana.onClick(() -> new AgregarPasaporteWindow(this, new Pasaje(), getModelObject()).open());

		
	}

}
