package aerolineas.ui;

import org.uqbar.commons.utils.Observable;

import TipoDeVuelo.VueloGeneral;

@Observable
public class ControllerDelVuelo {

	private VueloGeneral componenteSeleccionado;
	
	public VueloGeneral getComponenteSeleccionado(){
		return this.componenteSeleccionado;
	}

	public void setComponenteSeleccionado(VueloGeneral componenteSeleccionado) {
		this.componenteSeleccionado = componenteSeleccionado;
	}
	
}
